<?php

declare(strict_types=1);

namespace Aahoteev\MyFirstPackage;

class HelloWorld
{
    public function say(): string
    {
        return 'Hello world';
    }
}